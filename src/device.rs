use crate::info_provider::InfoProvider;

pub struct SmartSocket<'a> {
    pub name: &'a str,
}

impl InfoProvider for SmartSocket<'_> {
    fn get_state(&self) -> String {
        self.name.to_string()
    }
}

pub struct SmartThermometer<'a> {
    pub name: &'a str,
}

impl InfoProvider for SmartThermometer<'_> {
    fn get_state(&self) -> String {
        self.name.to_string()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn smart_socket_get_state() {
        let socket = SmartSocket { name: "socket" };

        assert!(socket.get_state().contains("socket"));
    }

    #[test]
    fn smart_thermometer_get_state() {
        let thermo = SmartThermometer { name: "thermo" };

        assert!(thermo.get_state().contains("thermo"));
    }
}
