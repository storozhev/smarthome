use crate::{error::Error as SmartHomeError, info_provider::InfoProvider};
use std::collections::HashMap;
use std::error::Error;
use std::fmt::Debug;

pub type Devices<'a> = HashMap<&'a str, &'a dyn InfoProvider>;

pub struct Room<'a> {
    pub name: &'a str,
    devices: Devices<'a>,
}

impl<'a> Room<'a> {
    #![allow(dead_code)]

    pub fn new(name: &'a str) -> Room {
        Self {
            name,
            devices: Devices::new(),
        }
    }

    pub fn add_device(
        &mut self,
        name: &'a str,
        device: &'a dyn InfoProvider,
    ) -> Result<(), Box<dyn Error>> {
        match self.devices.contains_key(name) {
            true => Err(Box::new(SmartHomeError::DeviceAlreadyExists)),
            _ => {
                self.devices.insert(name, device);
                Ok(())
            }
        }
    }

    pub fn get_device(&self, name: &'a str) -> Result<&'a dyn InfoProvider, Box<dyn Error>> {
        self.devices
            .get(name)
            .cloned()
            .ok_or_else(|| Box::new(SmartHomeError::DeviceNotFound) as _)
    }

    pub fn get_devices(&self) -> Vec<&str> {
        self.devices.iter().map(|(&k, _)| k).collect()
    }

    pub fn remove_device(&mut self, name: &'a str) -> Option<&'a dyn InfoProvider> {
        self.devices.remove_entry(name).map(|(_, v)| v)
    }
}

impl InfoProvider for Room<'_> {
    fn get_state(&self) -> String {
        format!(
            "{}{}",
            self.name,
            self.devices
                .iter()
                .map(|(_, d)| { format!("\n\t- {}", d.get_state()) })
                .collect::<Vec<_>>()
                .join("")
        )
    }
}

impl PartialEq for Room<'_> {
    fn eq(&self, other: &Self) -> bool {
        self.get_state() == other.get_state()
    }
}

impl Debug for Room<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "[{}]", self.name)
    }
}

#[cfg(test)]
mod test {
    use super::{Devices, Room, SmartHomeError};
    use crate::device::{SmartSocket, SmartThermometer};
    use crate::info_provider::InfoProvider;

    #[test]
    fn room_get_state() {
        let devices: [(&str, &dyn InfoProvider); 2] = [
            ("thermo", &SmartThermometer { name: "thermo" }),
            ("socket", &SmartSocket { name: "socket" }),
        ];

        let state = Room {
            name: "room",
            devices: Devices::from(devices),
        }
        .get_state();

        assert!(
            ["room", "socket", "thermo"]
                .into_iter()
                .all(|v| state.contains(v)),
            "state must contain room's name and device's names"
        );
    }

    #[test]
    fn room_add_device_with_unique_names() {
        let mut room = Room {
            name: "room",
            devices: Devices::new(),
        };

        let thermo1 = SmartThermometer { name: "thermo1" };
        let thermo2 = SmartThermometer { name: "thermo2" };

        assert!(room.add_device("thermo1", &thermo1).is_ok());
        assert!(room.add_device("thermo2", &thermo2).is_ok());
        assert_eq!(2, room.devices.len());
    }

    #[test]
    fn room_add_device_with_the_same_name() {
        let mut room = Room {
            name: "room",
            devices: Devices::new(),
        };

        let thermo = SmartThermometer { name: "thermo" };
        let socket = SmartSocket { name: "socket" };

        assert!(room.add_device("name", &thermo).is_ok());
        assert!(room
            .add_device("name", &socket)
            .unwrap_err()
            .downcast::<SmartHomeError>()
            .is_ok());
        assert_eq!(1, room.devices.len());
    }

    #[test]
    fn room_get_device() {
        const VALID_NAME: &str = "valid";
        const INVALID_NAME: &str = "invalid";
        const THERMO: SmartThermometer = SmartThermometer { name: VALID_NAME };

        let devices: [(&str, &dyn InfoProvider); 1] = [(VALID_NAME, &THERMO)];

        let room = Room {
            name: "",
            devices: Devices::from(devices),
        };

        struct TestCase<'a> {
            device_name: &'a str,
            is_ok: bool,
        }

        let test_cases = [
            TestCase {
                device_name: VALID_NAME,
                is_ok: true,
            },
            TestCase {
                device_name: INVALID_NAME,
                is_ok: false,
            },
        ];

        for tc in test_cases {
            assert_eq!(tc.is_ok, room.get_device(tc.device_name).is_ok());
        }
    }

    #[test]
    fn room_get_devices() {
        let devices: [(&str, &dyn InfoProvider); 2] = [
            ("foo", &SmartSocket { name: "socket" }),
            ("bar", &SmartThermometer { name: "thermo" }),
        ];

        let room = Room {
            name: "",
            devices: Devices::from(devices),
        };

        assert!(
            ["foo", "bar"]
                .iter()
                .all(|v| room.get_devices().contains(v)),
            "room must contain all given devices"
        );
    }

    #[test]
    fn room_remove_device_successfully() {
        const SOCKET: SmartSocket = SmartSocket { name: "socket" };
        let devices: [(&str, &dyn InfoProvider); 1] = [("foo", &SOCKET)];

        let mut room = Room {
            name: "",
            devices: Devices::from(devices),
        };

        assert_eq!(Some(&SOCKET as _), room.remove_device("foo"));
        assert_eq!(0, room.devices.len());
    }

    #[test]
    fn room_remove_device_failed() {
        const SOCKET: SmartSocket = SmartSocket { name: "socket" };
        let devices: [(&str, &dyn InfoProvider); 1] = [("foo", &SOCKET)];

        let mut room = Room {
            name: "",
            devices: Devices::from(devices),
        };

        // bar is an incorrect name so after trying to remove a device
        // by this name we expect None
        assert_eq!(None, room.remove_device("bar"));
        assert_eq!(1, room.devices.len());
    }
}
