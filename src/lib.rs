pub mod device;
pub mod error;
pub mod info_provider;
pub mod room;

use crate::info_provider::InfoProvider;
use error::Error as SmartHomeError;
use room::Room;
use std::collections::HashMap;
use std::error::Error;

pub type Rooms<'a> = HashMap<&'a str, &'a Room<'a>>;

/**
```
    use smart_home::SmartHome;
    use smart_home::device::{SmartSocket, SmartThermometer};
    use smart_home::info_provider::{
        create_report,
        InfoProvider,
        OwningDeviceInfoProvider,
        BorrowingDeviceInfoProvider,
    };
    use smart_home::room::Room;

    let socket1 = SmartSocket {
        name: "socket1".into(),
    };
    let socket2 = SmartSocket {
        name: "socket2".into(),
    };
    let thermo = SmartThermometer {
        name: "thermometer1".into(),
    };

    let info_provider_1 = OwningDeviceInfoProvider { socket: socket1 };

    let info_provider_2 = BorrowingDeviceInfoProvider {
        socket: &socket2,
        thermo: &thermo,
    };

    let thermo_in_room_1 = SmartThermometer {
        name: "my thermo in room 1".into(),
    };
    let thermo_in_room_2 = SmartThermometer {
        name: "my thermo in room 2".into(),
    };

    let mut room1 = Room::new("my room1");
    let mut room2 = Room::new("my room2");

    room1.add_device("my thermo", &thermo_in_room_1);
    room2.add_device("my thermo", &thermo_in_room_2);

    let mut house = SmartHome::new("my smart home");
    house.add_room(&room1);
    house.add_room(&room2);

    println!("Report #1: {}\n", create_report(&info_provider_1));
    println!("Report #2: {}\n", create_report(&info_provider_2));
    println!("Report #3: {}\n", create_report(&house));
```
*/
pub struct SmartHome<'a> {
    name: &'a str,
    rooms: Rooms<'a>,
}

impl<'a> SmartHome<'a> {
    #![allow(dead_code)]

    pub fn new(name: &'a str) -> Self {
        Self {
            name,
            rooms: Rooms::new(),
        }
    }

    pub fn add_room(&mut self, room: &'a Room) -> Result<(), Box<dyn Error>> {
        match self.rooms.contains_key(room.name) {
            true => Err(Box::new(SmartHomeError::RoomAlreadyExists)),
            _ => {
                self.rooms.insert(room.name, room);
                Ok(())
            }
        }
    }

    pub fn get_rooms(&self) -> Vec<&str> {
        self.rooms.iter().map(|(&k, _)| k).collect()
    }

    pub fn remove_room(&mut self, name: &'a str) -> Option<&'a Room> {
        self.rooms.remove_entry(name).map(|(_, v)| v)
    }
}

impl InfoProvider for SmartHome<'_> {
    fn get_state(&self) -> String {
        format!(
            "\n{}{}",
            self.name,
            self.rooms
                .iter()
                .map(|(_, r)| { format!("\n- {}", r.get_state()) })
                .collect::<Vec<_>>()
                .join("")
        )
    }
}

#[cfg(test)]
mod test {
    use crate::{
        error::Error as SmartHomeError, info_provider::InfoProvider, Room, Rooms, SmartHome,
    };

    #[test]
    fn home_add_room_with_unique_room_names() {
        let mut home = SmartHome {
            name: "",
            rooms: Rooms::new(),
        };

        let room1 = Room::new("room1");
        let room2 = Room::new("room2");

        assert!(home.add_room(&room1).is_ok());
        assert!(home.add_room(&room2).is_ok());
        assert_eq!(2, home.rooms.len());
    }

    #[test]
    fn home_add_room_with_the_same_room_names() {
        let mut home = SmartHome {
            name: "",
            rooms: Rooms::new(),
        };

        let room1 = Room::new("room");
        let room2 = Room::new("room");

        assert!(home.add_room(&room1).is_ok());
        assert!(home
            .add_room(&room2)
            .unwrap_err()
            .downcast::<SmartHomeError>()
            .is_ok());
        assert_eq!(1, home.rooms.len());
    }

    #[test]
    fn home_get_rooms() {
        let room1 = Room::new("");
        let room2 = Room::new("");

        let home = SmartHome {
            name: "home",
            rooms: [("room1", &room1), ("room2", &room2)].into(),
        };

        let got_rooms = home.get_rooms();

        assert_eq!(2, got_rooms.len());
        assert!(
            ["room1", "room2"].iter().all(|v| got_rooms.contains(v)),
            "home must contain all given rooms"
        );
    }

    #[test]
    fn home_get_state() {
        let room1 = Room::new("room1");
        let room2 = Room::new("room2");

        let state = SmartHome {
            name: "home",
            rooms: [("room1", &room1), ("room2", &room2)].into(),
        }
        .get_state();

        assert!(
            ["home", "room1", "room2"].iter().all(|v| state.contains(v)),
            "home's state must contain its name and all of the rooms' names"
        );
    }

    #[test]
    fn home_remove_room_successfully() {
        let room: Room = Room::new("");

        let mut home = SmartHome {
            name: "",
            rooms: [("room", &room)].into(),
        };

        assert_eq!(Some(&room), home.remove_room("room"));
    }

    #[test]
    fn home_remove_room_failed() {
        let room: Room = Room::new("");

        let mut home = SmartHome {
            name: "",
            rooms: [("room", &room)].into(),
        };

        // bar is an incorrect name so after trying to remove a room
        // by this name we expect None
        assert_eq!(None, home.remove_room("bar"));
    }
}
