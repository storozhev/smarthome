use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("room already exists")]
    RoomAlreadyExists,
    #[error("device not found")]
    DeviceNotFound,
    #[error("device already exists")]
    DeviceAlreadyExists,
}
