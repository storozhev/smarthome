use std::fmt::Debug;

use crate::device::{SmartSocket, SmartThermometer};

pub trait InfoProvider {
    fn get_state(&self) -> String;
}

impl Debug for dyn InfoProvider {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.get_state())
    }
}

impl PartialEq for dyn InfoProvider {
    fn eq(&self, other: &Self) -> bool {
        self.get_state() == other.get_state()
    }
}

#[allow(dead_code)]
pub fn create_report<T>(provider: &T) -> String
where
    T: InfoProvider,
{
    provider.get_state()
}

pub struct OwningDeviceInfoProvider<'a> {
    pub socket: SmartSocket<'a>,
}

impl InfoProvider for OwningDeviceInfoProvider<'_> {
    fn get_state(&self) -> String {
        format!("\n- {}", self.socket.name)
    }
}

pub struct BorrowingDeviceInfoProvider<'a, 'b> {
    pub socket: &'a SmartSocket<'a>,
    pub thermo: &'b SmartThermometer<'b>,
}

impl InfoProvider for BorrowingDeviceInfoProvider<'_, '_> {
    fn get_state(&self) -> String {
        [self.socket.get_state(), self.thermo.get_state()]
            .map(|s| format!("\n- {}", s))
            .join("")
    }
}

#[cfg(test)]
mod test {
    use super::{
        BorrowingDeviceInfoProvider, InfoProvider, OwningDeviceInfoProvider, SmartSocket,
        SmartThermometer,
    };

    #[test]
    fn owning_device_info_provider_get_state() {
        let state = OwningDeviceInfoProvider {
            socket: SmartSocket { name: "socket" },
        }
        .get_state();
        assert!(
            state.contains("socket"),
            "state must contain the name of a device",
        );
    }

    #[test]
    fn borrowing_device_info_provider_get_state() {
        let state = BorrowingDeviceInfoProvider {
            socket: &SmartSocket { name: "socket" },
            thermo: &SmartThermometer { name: "thermo" },
        }
        .get_state();
        assert!(
            ["socket", "thermo"].into_iter().all(|v| state.contains(v)),
            "state must contain the name of a devices",
        );
    }
}
